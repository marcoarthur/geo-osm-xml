# NAME

Geo::OSM::Tag - A Tag Entity

# SYNOPSIS

    use Geo::OSM::Tag;

# DESCRIPTION

Geo::OSM::Tag represents a Tag for OSM. It has two main attributes: a key and a
value.

# AUTHOR

Marco Arthur <arthurpbs@gmail.com>

# COPYRIGHT

Copyright 2018- Marco Arthur

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
