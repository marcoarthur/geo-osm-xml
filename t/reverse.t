use strict;
use warnings;

use Test::More;
use Geo::OSM::XML;
use List::Util qw( first );

my $g = Geo::OSM::XML->new( file => 't/data/map.osm' );
my $nodes = $g->nodes;
my $n = first { scalar @{ $_->tags } > 0 } @$nodes;
my $address = $n->address;
ok defined $address, "Resolved address";
note explain($address);

done_testing;
