use strict;
use Test::More;
use Geo::OSM::XML;
use List::Util qw( first );

# replace with the actual test
my $g = Geo::OSM::XML->new( file => 't/data/map.osm' );
isa_ok $g, 'Geo::OSM::XML';

can_ok $g, qw(nodes);

my $nodes = $g->nodes;
ok scalar(@$nodes) > 0, 'has more than zero nodes';
note "Total of nodes: " . scalar(@$nodes);

# first node with tags
my $n = first { scalar @{ $_->tags } > 0 } @$nodes;

# attributes we must find in nodes
can_ok $n, qw( tags id user lat lon );

# check values of these attributes
foreach my $att ( qw( id user lat lon ) ) {
	ok $n->$att, "$att is " . $n->$att;
}

# get tag values
can_ok $n->tags->[0], qw( key value );

# check values of these attributes
foreach my $att ( qw( key value ) ) {
	ok $n->tags->[0]->$att, "$att is " . $n->tags->[0]->$att;
}

# Store node as JSON file
$n->store('out.json');

done_testing;
