requires 'Geo::Coder::XYZ';
requires 'Moose::Role';
requires 'Moose::Util';
requires 'MooseX::AttributeShortcuts';
requires 'MooseX::Storage';
requires 'XML::Rabbit';
requires 'XML::Rabbit::Root';
requires 'perl', '5.010';

on configure => sub {
    requires 'Module::Build::Tiny', '0.034';
};

on test => sub {
    requires 'List::Util';
    requires 'Test::More';
};


