package Geo::OSM::XML;

use strict;
use warnings;
use 5.010;
our $VERSION = '0.01';

use XML::Rabbit::Root;
use MooseX::AttributeShortcuts;

add_xpath_namespace osm => 'http://www.openstreetmap.org/xml';

has_xpath_object_list 'nodes' => '/osm/node' => 'Geo::OSM::Node';

finalize_class();

1;
__END__

=encoding utf-8

=head1 NAME

Geo::OSM::XML - Module that reads from OSM XML file

=head1 SYNOPSIS

  use Geo::OSM::XML;

  my $data = Geo::OSM::XML->new( file => 'osm.xml' );
  my @nodes = $data->nodes;

=head1 DESCRIPTION

Geo::OSM::XML is

=head1 AUTHOR

Marco Arthur E<lt>arthurpbs@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2018- Marco Arthur

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
