package Geo::OSM::Storage::Traits::PublicOnly;

use Moose::Role;
use MooseX::Storage;
use MooseX::AttributeShortcuts;
use Moose::Util qw( apply_all_roles );

with Storage( format => 'JSON', io => 'File' );
our $VERSION = '0.01';

before 'store' => sub {
    my ($self) = @_;

    my $meta           = $self->meta;
    my $dont_serialize = 'MooseX::Storage::Meta::Attribute::Trait::DoNotSerialize';

    # Private attributes that needs to be marked as ``DoNotSerialize''
    my @private_attr =
      grep { $_->name =~ /^_/ && !$_->does($dont_serialize) } $meta->get_all_attributes;

    # Apply the role to the attribute
    apply_all_roles( $_, $dont_serialize ) for @private_attr;

};

1;
__END__

=encoding utf-8

=head1 NAME

Geo::OSM::Storage::Traits::PublicOnly - Trait for Serialization of Entities
of OSM XML data schema.

=head1 SYNOPSIS

  use Geo::OSM::Storage::Traits::PublicOnly;

=head1 DESCRIPTION

Geo::OSM::Storage::Traits::PublicOnly is a Trait for storage of entities parsed
and transformed as Moose Object from OSM XML file or API call. It can store
in any format supported by L<MooseX::Storage>.

=head1 AUTHOR

Marco Arthur E<lt>arthurpbs@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2018- Marco Arthur

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

L<MooseX::Storage>
=cut
