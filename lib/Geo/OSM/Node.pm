package Geo::OSM::Node;

use strict;
use 5.010;
our $VERSION = '0.01';

use XML::Rabbit;
with 'Geo::OSM::Storage::Traits::PublicOnly';

has_xpath_value id => './@id';
has_xpath_value user => './@user';
has_xpath_value lat => './@lat';
has_xpath_value lon => './@lon';

# TODO: Explain why this needs to be here. Seems lat and lon methods are
# visible only after this evaluation point
with 'Geo::OSM::GeoCode::Traits::ReverseCoder';

has_xpath_object_list 'tags' => './/tag' => 'Geo::OSM::Tag';

finalize_class();

1;

__END__

=encoding utf-8

=head1 NAME

Geo::OSM::Node - Get Node information OSM XML data

=head1 SYNOPSIS

  use Geo::OSM::Node;

=head1 DESCRIPTION

Geo::OSM::Node is

=head1 AUTHOR

Marco Arthur E<lt>arthurpbs@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2018- Marco Arthur

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
