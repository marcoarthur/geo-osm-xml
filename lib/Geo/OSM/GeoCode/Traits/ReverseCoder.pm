package Geo::OSM::GeoCode::Traits::ReverseCoder;

use Moose::Role;
use Geo::Coder::XYZ;
use MooseX::AttributeShortcuts;
our $VERSION = '0.01';

requires qw( lat lon );

has address => (
	is => 'lazy',
	isa => 'HashRef',
);

sub _build_address {
	my $self = shift;
	my $coords = join ',', $self->lat, $self->lon;
	my $address = 
		Geo::Coder::XYZ->new->reverse_geocode( latlng => $coords );

	return $address;
}

1;

__END__

=encoding utf-8

=head1 NAME

Geo::OSM::GeoCode::Traits::ReverseCoder - Reverse Geocoder Trait

=head1 SYNOPSIS

  use Geo::OSM::GeoCode::Traits::ReverseCoder;

=head1 DESCRIPTION

Geo::OSM::GeoCode::Traits::ReverseCoder adds address attribute given
by a reverse geocoder of choice. Currently only support L<Geo::Coder::XYZ>

It adds an C<address> attribute to the class, this is lazy evaluated using
the online GeoCoder.

=head1 AUTHOR

Marco Arthur E<lt>arthurpbs@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2018- Marco Arthur

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
